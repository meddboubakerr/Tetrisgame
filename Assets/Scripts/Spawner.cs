using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameObject[] tetrisPrephab;
    [SerializeField] float SecondSpawn = 0.25f;
    [SerializeField] float minTras;
    [SerializeField] float maxTras;

    // Start is called before the first frame update
     void Start()
     {
        StartCoroutine(tetrisSpawn());
     }


    IEnumerator tetrisSpawn()
    {
        
        while (true)
        {
            
            var wanted = Random.Range(minTras, maxTras);
            var position = new Vector3(wanted, transform.position.y);
            GameObject gameObject = Instantiate(tetrisPrephab[Random.Range(0, tetrisPrephab.Length)] , position, Quaternion.identity);
            yield return new WaitForSeconds(SecondSpawn);
            Destroy(gameObject, 5f);
            
        }
    }

    
}
