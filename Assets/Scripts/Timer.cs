using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Timer : MonoBehaviour
{
    
    public bool gameOver=false;
    public float currentTime;
    public float startingTime = 10f;
    public Timer timer;

    [SerializeField] TextMeshProUGUI countdownText;
    public GameObject GameOver;
    
    void Start()
    {
       
        currentTime = startingTime;
    }
    void Update()
    {
        
            currentTime -= 1 * Time.deltaTime;
            countdownText.text = currentTime.ToString("0");

        if (currentTime <= 0) 
        {
            currentTime = 0;
            gameOver = true;
            

        }

        if (timer.gameOver)
        {
            GameOver.SetActive(true);
            
      
        }
       
    }
}
